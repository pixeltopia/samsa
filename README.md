# Samsa

![samsa](docs/samsa.png)

This is is a little project that uses the Typescript Compiler API to inspect type definitions and output some code.

## Why the name?

This project is named after _Gregor Samsa_ who is a character that transforms into a Beetle in the story [The Metamorphosis](https://en.wikipedia.org/wiki/The_Metamorphosis) by Franz Kafka. It's also the name of a savoury pastry with interesting fillings.
