module.exports = {
    clearMocks: true,
    collectCoverageFrom: ['src/**/*.ts'],
    coverageDirectory: 'coverage',
    coverageReporters: ['text-summary', 'text'],
    coverageThreshold: {
        global: {
            branches: 0,
            functions: 0,
            lines: 0,
            statements: 0,
        },
    },
    coveragePathIgnorePatterns: ['/node_modules/', '/.vscode/'],
    watchPathIgnorePatterns: ['/node_modules/', '/.vscode/'],
    testMatch: ['**/?(*.)test.ts'],
    moduleFileExtensions: ['ts', 'js', 'json', 'node'],
    transform: { '^.+\\.tsx?$': 'ts-jest' },
    globals: {
    },
    verbose: false,
    timers: 'fake',
    setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
};
