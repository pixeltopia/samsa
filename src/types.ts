interface A {
    b: B[];
    c: number[];
    d?: string;
}

type B = C | D | E;

interface C {
    type: 'c';
    data: string;
}

interface D {
    type: 'd';
    data: number;
}

interface E {
    type: 'e';
    data: F;
}

interface F {
    y: G;
}

enum H {
    Foo = 'Foo',
    Bar = 'Bar',
    Baz = 'Baz',
}

type G = Record<string, B>;

enum I {
    Foo = 1,
    Bar = 2,
    Baz = 3,
}
