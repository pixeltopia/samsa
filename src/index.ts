import * as ts from 'typescript';
import fs from 'fs';

const debug = false;

let indent = 0;
const show = (node: ts.Node) => {
    console.log(new Array(indent).join(' ') + ts.SyntaxKind[node.kind]);
};

const file = fs.readFileSync('src/types.ts', { encoding: 'utf8' });
const root = ts.createSourceFile('types.ts', file, ts.ScriptTarget.ES2019);

const tokens: string[] = [];

const puts = (it: string) => tokens.push(it);

const recur = (node: ts.Node, check: (it: ts.Node) => boolean = () => true) => {
    indent++;
    ts.forEachChild(node, (it) => {
        if (check(it)) {
            handle(it);
        }
    });
    indent--;
};

const not = (fn: (it: ts.Node) => boolean) => (it: ts.Node) => !fn(it);

const kinds =
    (...kinds: ts.SyntaxKind[]) =>
    (it: ts.Node) =>
        kinds.includes(it.kind);

const stringy = kinds(ts.SyntaxKind.StringKeyword, ts.SyntaxKind.StringLiteral);
const numbery = kinds(
    ts.SyntaxKind.NumberKeyword,
    ts.SyntaxKind.NumericLiteral,
);
const booley = kinds(
    ts.SyntaxKind.BooleanKeyword,
    ts.SyntaxKind.TrueKeyword,
    ts.SyntaxKind.FalseKeyword,
);

const lowerCase = (it: string) =>
    it.substring(0, 1).toLowerCase() + it.substring(1);

const isRecord = (it: ts.TypeReferenceNode) =>
    it.typeName.getText(root) === 'Record';

const handle = (node: ts.Node) => {
    if (debug) {
        show(node);
    }

    if (booley(node)) {
        puts('Bool');
    }

    if (numbery(node)) {
        puts('Double');
    }

    if (stringy(node)) {
        puts('String');
    }

    if (ts.isSourceFile(node)) {
        recur(node);
    }

    if (ts.isIdentifier(node)) {
        puts(node.text);
    }

    if (ts.isArrayTypeNode(node)) {
        puts('[');
        recur(node);
        puts(']');
    }

    if (ts.isTypeReferenceNode(node)) {
        if (isRecord(node)) {
            puts('[');
            const [lhs, rhs] = node.typeArguments || [];
            handle(lhs);
            puts(':');
            handle(rhs);
            puts(']');
        } else {
            recur(node);
        }
    }

    if (ts.isLiteralTypeNode(node)) {
        handle(node.literal);
    }

    if (ts.isInterfaceDeclaration(node)) {
        puts('struct');
        recur(node, ts.isIdentifier);
        puts('{');
        recur(node, ts.isPropertySignature);
        puts('}');
    }

    if (ts.isTypeAliasDeclaration(node)) {
        const [union] = node.getChildren(root).filter(ts.isUnionTypeNode);
        if (union) {
            puts('enum');
            recur(node, ts.isIdentifier);
            puts('{');

            const names = union.types
                .filter(ts.isTypeReferenceNode)
                .map(({ typeName }) => typeName)
                .filter(ts.isIdentifier)
                .map(({ text }) => text);

            for (const next of names) {
                puts('case');
                puts(lowerCase(next));
                puts('(');
                puts(next);
                puts(')');
            }

            puts('}');
        } else {
            puts('typealias');
            recur(node, ts.isIdentifier);
            puts('=');
            recur(node, not(ts.isIdentifier));
        }
    }

    if (ts.isEnumDeclaration(node)) {
        puts('enum');
        recur(node, ts.isIdentifier);

        const types = node.members
            .map(({ initializer }) => initializer)
            .filter((it): it is ts.Expression => !!it);

        const stringy = types.some(ts.isStringLiteral);
        const numbery = types.some(ts.isNumericLiteral);

        if ((stringy && numbery) || (!stringy && !numbery)) {
            console.error('enums must be either numbers or strings');
            process.exit(1);
        }

        puts(':');

        if (stringy) {
            puts('String');
        } else {
            puts('Int');
        }

        puts('{');

        for (const { name, initializer } of node.members) {
            puts('case');
            if (ts.isIdentifier(name)) {
                puts(lowerCase(name.text));
            }
            puts('=');
            if (initializer && ts.isStringLiteral(initializer)) {
                puts(`"${initializer.text}"`);
            }
            if (initializer && ts.isNumericLiteral(initializer)) {
                puts(initializer.text);
            }
        }

        puts('}');
    }

    if (ts.isEnumMember(node)) {
        recur(node);
    }

    if (ts.isPropertySignature(node)) {
        puts('let');
        recur(node, ts.isIdentifier);
        puts(':');
        recur(node, not(ts.isIdentifier));
        if (node.questionToken) {
            puts('?');
        }
    }
};

handle(root);

const output = tokens
    .join(' ')
    .replace(/\[\s(.*?)\s\]/g, '[$1]')
    .replace(/\s\:/g, ':')
    .replace(/\s\?/g, '?')
    .replace(/let/g, '\n let')
    .replace(/struct/g, '\nstruct')
    .replace(/typealias/g, '\ntypealias')
    .replace(/enum/g, '\nenum')
    .replace(/case/g, '\n case')
    .replace(/case (.+) \( (.+) \)/g, 'case $1($2)')
    .replace(/\}/g, '\n}\n');

console.log(output);
